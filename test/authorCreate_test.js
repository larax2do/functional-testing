"use strict";

const assert = require("assert");

let webdriver = require("selenium-webdriver");

let By = webdriver.By;
let until = webdriver.until;

describe("Crear autores", () => {
  let driver = new webdriver.Builder()
    .withCapabilities(webdriver.Capabilities.chrome())
    .build();

  it("Crear autor solo con nombre(incorrecto)", (done) => {
    driver
      .get("http://localhost:3000/catalog")
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[1]/ul/li[7]/a"))
      )
      .then((allBooks) => allBooks.click())
      .then(() =>
        driver.findElement(
          By.xpath("/html/body/div/div/div[2]/form/div[1]/input[1]")
        )
      )
      .then((entrada) => entrada.sendKeys("Thales"))
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[2]/form/button"))
      )
      .then((button) => button.click())
      .then(() => driver.getCurrentUrl())
      .then((actual) => {
        const expected = "http://localhost:3000/catalog/author/create";
        assert.strictEqual(actual, expected);
      })
      .then(done)
      .catch((err) => done(err));
  });
  it("Crear autor solo con apellido(incorrecto)", (done) => {
    driver
      .get("http://localhost:3000/catalog")
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[1]/ul/li[7]/a"))
      )
      .then((allBooks) => allBooks.click())
      .then(() =>
        driver.findElement(
          By.xpath("/html/body/div/div/div[2]/form/div[1]/input[2]")
        )
      )
      .then((entrada) => entrada.sendKeys("Ravenhold"))
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[2]/form/button"))
      )
      .then((button) => button.click())
      .then(() => driver.getCurrentUrl())
      .then((actual) => {
        const expected = "http://localhost:3000/catalog/author/create";
        assert.strictEqual(actual, expected);
      })
      .then(done)
      .catch((err) => done(err));
  });

  it("Crear autor sin ningun dato(incorrecto)", (done) => {
    driver
      .get("http://localhost:3000/catalog")
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[1]/ul/li[7]/a"))
      )
      .then((allBooks) => allBooks.click())
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[2]/form/button"))
      )
      .then((button) => button.click())
      .then(() => driver.getCurrentUrl())
      .then((actual) => {
        const expected = "http://localhost:3000/catalog/author/create";
        assert.strictEqual(actual, expected);
      })
      .then(done)
      .catch((err) => done(err));
  });

  it("Crear autor con solo con nombre y apellidos(correcto)", (done) => {
    driver
      .get("http://localhost:3000/catalog")
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[1]/ul/li[7]/a"))
      )
      .then((allBooks) => allBooks.click())
      .then(() =>
        driver.findElement(
          By.xpath("/html/body/div/div/div[2]/form/div[1]/input[1]")
        )
      )
      .then((entrada) => entrada.sendKeys("Thales"))
      .then(() =>
        driver.findElement(
          By.xpath("/html/body/div/div/div[2]/form/div[1]/input[2]")
        )
      )
      .then((entrada) => entrada.sendKeys("Ravenhold"))
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[2]/form/button"))
      )
      .then((button) => button.click())
      .then(() => driver.findElement(By.xpath("/html/body/div/div/div[2]/h1")))
      .then((message) => message.getText())
      .then((actual) => {
        const expected = "Author: Ravenhold, Thales";
        assert.strictEqual(actual, expected);
      })
      .then(done)
      .catch((err) => done(err));
  });

  it("Crear autor sin fecha de defuncion(correcto)", (done) => {
    driver
      .get("http://localhost:3000/catalog")
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[1]/ul/li[7]/a"))
      )
      .then((allBooks) => allBooks.click())
      .then(() =>
        driver.findElement(
          By.xpath("/html/body/div/div/div[2]/form/div[1]/input[1]")
        )
      )
      .then((entrada) => entrada.sendKeys("Thales"))
      .then(() =>
        driver.findElement(
          By.xpath("/html/body/div/div/div[2]/form/div[1]/input[2]")
        )
      )
      .then((entrada) => entrada.sendKeys("Ravenhold"))
      .then(() => 
        driver.findElement(
            By.xpath("/html/body/div/div/div[2]/form/div[2]/input")
        )
      )
      .then((entrada) =>entrada.sendKeys("22071998"))
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[2]/form/button"))
      )
      .then((button) => button.click())
      .then(() => driver.findElement(By.xpath("/html/body/div/div/div[2]/h1")))
      .then((message) => message.getText())
      .then((actual) => {
        const expected = "Author: Ravenhold, Thales";
        assert.strictEqual(actual, expected);
      })
      .then(() => driver.findElement(By.xpath("/html/body/div/div/div[2]/p[1]")))
      .then((message) =>message.getText())
      .then((actual) =>{
          const expected="22 de jul. de 1998 -";
          assert.strictEqual(actual,expected);
      })
      
      .then(done)
      .catch((err) => done(err));
  });

  it("Crear autor sin fecha de nacimiento(correcto)", (done) => {
    driver
      .get("http://localhost:3000/catalog")
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[1]/ul/li[7]/a"))
      )
      .then((allBooks) => allBooks.click())
      .then(() =>
        driver.findElement(
          By.xpath("/html/body/div/div/div[2]/form/div[1]/input[1]")
        )
      )
      .then((entrada) => entrada.sendKeys("Thales"))
      .then(() =>
        driver.findElement(
          By.xpath("/html/body/div/div/div[2]/form/div[1]/input[2]")
        )
      )
      .then((entrada) => entrada.sendKeys("Ravenhold"))
      .then(() => 
        driver.findElement(
            By.xpath("/html/body/div/div/div[2]/form/div[3]/input")
        )
      )
      .then((entrada) =>entrada.sendKeys("07052095"))
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[2]/form/button"))
      )
      .then((button) => button.click())
      .then(() => driver.findElement(By.xpath("/html/body/div/div/div[2]/h1")))
      .then((message) => message.getText())
      .then((actual) => {
        const expected = "Author: Ravenhold, Thales";
        assert.strictEqual(actual, expected);
      })
      .then(() => driver.findElement(By.xpath("/html/body/div/div/div[2]/p[1]")))
      .then((message) =>message.getText())
      .then((actual) =>{
          const expected="- 7 de may. de 2095";
          assert.strictEqual(actual,expected);
      })
      .then(done)
      .catch((err) => done(err));
  });
  it("Crear autor con datos completos(correcto)", (done) => {
    driver
      .get("http://localhost:3000/catalog")
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[1]/ul/li[7]/a"))
      )
      .then((allBooks) => allBooks.click())
      .then(() =>
        driver.findElement(
          By.xpath("/html/body/div/div/div[2]/form/div[1]/input[1]")
        )
      )
      .then((entrada) => entrada.sendKeys("Thales"))
      .then(() =>
        driver.findElement(
          By.xpath("/html/body/div/div/div[2]/form/div[1]/input[2]")
        )
      )
      .then((entrada) => entrada.sendKeys("Ravenhold"))
      .then(() => 
        driver.findElement(
            By.xpath("/html/body/div/div/div[2]/form/div[2]/input")
        )
      )
      .then((entrada) =>entrada.sendKeys("22071998"))
      .then(() => 
        driver.findElement(
            By.xpath("/html/body/div/div/div[2]/form/div[3]/input")
        )
      )
      .then((entrada) =>entrada.sendKeys("07052095"))
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[2]/form/button"))
      )
      .then((button) => button.click())
      .then(() => driver.findElement(By.xpath("/html/body/div/div/div[2]/h1")))
      .then((message) => message.getText())
      .then((actual) => {
        const expected = "Author: Ravenhold, Thales";
        assert.strictEqual(actual, expected);
      })
      .then(() => driver.findElement(By.xpath("/html/body/div/div/div[2]/p[1]")))
      .then((message) =>message.getText())
      .then((actual) =>{
          const expected="22 de jul. de 1998 - 7 de may. de 2095";
          assert.strictEqual(actual,expected);
      })
      .then(() => driver.quit())
      .then(done)
      .catch((err) => done(err));
  });
});
